#version 330 core

in vec3 vertex;
in vec3 normal;

in vec3 matamb;
in vec3 matdiff;
in vec3 matspec;
in float matshin;

uniform mat4 proj;
uniform mat4 view;
uniform mat4 TG;

//uniform vec3 posFocus;
//uniform vec3 colFocus;
//uniform vec3 llumAmbient;

vec3 colFocus = vec3(0.8, 0.8, 0.8);
vec3 llumAmbient = vec3(0.2, 0.2, 0.2);
vec3 posFocus = vec3(0.0, 0.0, 0.0);

out vec3 fcolor;

vec3 Lambert (vec3 NormSCO, vec3 L) {
    vec3 colRes = llumAmbient * matamb1;
    if (dot (L, NormSCO) > 0)
      colRes = colRes + colFocus * matdiff1 * dot (L, NormSCO);
    return (colRes);
}

vec3 Phong (vec3 NormSCO, vec3 L, vec4 vertSCO) 
{
    vec3 colRes = Lambert (NormSCO, L);
    if (dot(NormSCO,L) < 0)
      return colRes;  // no hi ha component especular
    vec3 R = reflect(-L, NormSCO); // equival a: normalize (2.0*dot(NormSCO,L)*NormSCO - L);
    vec3 V = normalize(-vertSCO.xyz);
    if ((dot(R, V) < 0) || (matshin1 == 0))
      return colRes;  // no hi ha component especular
    float shine = pow(max(0.0, dot(R, V)), matshin1);
    return (colRes + matspec1 * colFocus * shine); 
}

void main()
{
    mat3 normalMatrix = inverse(transpose(mat3(view*TG)));
    vec3 normSCO = normalize(normalMatrix * normal);
    vec4 vertexSCO = view*TG*vec4(vertex,1.0);
    
    /* Depende si se pasa como uniform ya multiplicado o no
    vec4 focusSCA = view*vec4(posFocus, 1.0);
    vec4 focusSCO = vec4(posFocus, 1.0));
    vec4 focusModel = view*TG*(posFocus, 1.0));
    */
	
    vec3 LSCO = normalize(focus.xyz - vertexSCO.xyz);
    fcolor = Phong(normSCO, LSCO, vertexSCO);
    gl_Position = proj * view * TG * vec4(vertex, 1.0);
}
