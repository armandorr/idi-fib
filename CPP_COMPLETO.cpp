#include "MyGLWidget.h"

#define GLM_FORCE_RADIANS

//Include aunque deberia estar
#include "glm/gtc/matrix_inverse.hpp"
#include “glm/gtc/matrix_transform.hpp”

//Funcions predefinides
// Tri: radians(), degrees(), sin(), cos(), tan(), asin(), acos(), atan()
// Num: pow(), log(), exp(), abs(), sign(), floor(), min(), max()
// Geo: length(), distance(), dot(), cross(), normalize()

//Al .h
public slots:
  void changeValuePosFocus(const int); 
  //¡¡¡ Poner makeCurrent() i update() !!!
  //Suele ser necesario llamar a viewTransform, projectTransform o modelTransform despues
  //Radios:
  //emit(func(true))
  //emit(func(false)) setCheked(bool)

//Al .h
signals:
  void sendPosFocus(const int); //No se implementa
  
//Escribir para debuggar  
std::cerr << "Hola" << std::endl;

void MyGLWidget::PasarCosasShaders{
  matshinLoc = glGetAttribLocation (program->programId(), "matshin"); //IN
  transLoc = glGetUniformLocation (program->programId(), "TG"); //UNIFORM
  
  glUniform1f (varLoc, var); //variable (no se le pasa direccio sino directamente la variable)
  glUniform3fv (colFocusLoc, 1, &colFocus[0]); //vector
  glUniformMatrix4fv (viewLoc, 1, GL_FALSE, &View[0][0]); //matriz
}
  
void MyGLWidget::initializeGL ()
{
  // Cal inicialitzar l'ús de les funcions d'OpenGL
  initializeOpenGLFunctions();  
  glClearColor(0.05, 0.3, 0.1, 1.0); // defineix color de fons (d'esborrat)
  glEnable(GL_DEPTH_TEST);
  carregaShaders();
  iniEscena ();
  iniCamera ();
}

void MyGLWidget::iniEscena ()
{
  creaBuffersArc();
  creaBuffersPersonatge();
  creaBuffersTerra();

  centreEsc = glm::vec3 (3, 2, 3);
  radiEsc = 4.5;
}

void MyGLWidget::iniCamera ()
{
  angleY = angleX = 0.0;
  
  glEnable(GL_CULL_FACE); //activem o desactivem el back-face culling
  glDisable(GL_CULL_FACE); //a qualsevol lloc on es necessiti
  
  projectTransform ();
  viewTransform ();
}

void MyGLWidget::calculCentre+RadiEscena(glm::vec3 min, glm::vec3 max) 
{
  centreEsc = glm::vec3((max[0]+min[0])/2,(max[1]+min[1])/2,(max[2]+min[2])/2);
  radiEsc = glm::distance(min,max)/2; //radiEsc = sqrt(pow(max[0]-min[0],2)+pow(max[1]-min[1],2)+pow(max[2]-min[2],2))/2;
}

void MyGLWidget::paintGL () 
{
    // En cas de voler canviar els paràmetres del viewport, descomenteu la crida següent i
  // useu els paràmetres que considereu (els que hi ha són els de per defecte)
  //  glViewport (0, 0, ample, alt);
  
  // Esborrem el frame-buffer i el depth-buffer
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  //--------------------------------------------------------
  // Activem el VAO per a pintar el terra
  glBindVertexArray (VAO_Terra);  
  // pintem terra
  modelTransformTerra ();
  glDrawArrays(GL_TRIANGLES, 0, 12);

  //--------------------------------------------------------
  // Activem el VAO per a pintar el personatge
  glBindVertexArray (VAO_lego);
  // pintem el persontage
  modelTransform();
  glDrawArrays(GL_TRIANGLES, 0, legoModel.faces().size()*3);

  //--------------------------------------------------------
  glBindVertexArray(0);
}

void MyGLWidget::resizeGL (int w, int h)
{
  ample = w;
  alt = h;
  rav = float(ample)/float(alt);
  
  float angle = asin(0.5);
  
  //ortogonal i perspectiva
  left = -radiEsc;
  right = radiEsc;
  bottom = -radiEsc;
  top = radiEsc;
  if(rav < 1){
      left = -radiEsc;
      right = radiEsc;
      bottom = -radiEsc/rav;
      top = radiEsc/rav;
      
      fov = 2*atan(tan(angle)/rav);
  }
  else if(rav > 1){
      left = -radiEsc*rav;
      right = radiEsc*rav;
      bottom = -radiEsc;
      top = radiEsc;
      
      fov = 2*angle;
  }
  
  projectTransform(); //important el projectTransform()
}

void MyGLWidget::TotaEscenaSenseRetallar(){
    
  calculCentre+RadiEscena(punt_min, punt_max);
  
  d = 2*radiEsc;
  vrp = centreEsc;
  obs = centreEsc + d * glm::vec3(1, 0, 0); //d * direccio desde on mirem
  up = glm::vec3(0,1,0);
  
  float angle = asin(radiEsc/d); //asin(0.5)
  fov = angle*2;
  
  zn = radiEsc;
  zf = 3.0*radiEsc;
}

void MyGLWidget::calculNormalMatrixAlCpp()
{
    glm::mat3 normalMatrix = glm::inverseTranspose(glm::mat3(View*TG));
    glUniformMatrix4fv(normalMatrixLoc, 1, GL_FALSE, &normalMatrix[0][0]);
}

/*
 mat = glm::translate(mat, vec_trans);
 mat = glm::rotate(mat, float_angle, vec_eix_rotacio);
 mat = glm::scale(mat, vec_scale);
*/

void MyGLWidget::modelTransform()
{
  TG = glm::mat4(1.0f);
  TG = glm::translate(TG, posModel); //per moure un model a la seva posició incial
  TG = glm::rotate(TG, float(M_PI)/2, glm::vec3(0,1,0));
  TG = glm::scale(TG, glm::vec3(X*escala));
  TG = glm::translate(TG, -centreBase);
  glUniformMatrix4fv(transLoc, 1, GL_FALSE, &legoTG[0][0]);
}

void MyGLWidget::projectTransform ()
{
  glm::mat4 Proj;
  if(ortogonal == 1){ //Ortogonal
    Proj = glm::ortho(left, right, bottom, top, zn, zf);
  }
  else{ //Perspectiva
    if(primera_persona == 1){ //primera persona
        Proj = glm::perspective(angle_fix, rav, 0.1f, 30.0f);
    }
    else{ //tercera persona
        Proj = glm::perspective(fov, rav, zn, zf);
    }
  }
  glUniformMatrix4fv (projLoc, 1, GL_FALSE, &Proj[0][0]);
}

void MyGLWidget::viewTransform ()
{
  glm::mat4 View(1.f);
  if(primera_persona == 1){ //primera persona
     //glm::vec3 OBS = posModelo + glm::vec3(0, posCamara[=alturaModelo + X sobre la cabeza], 0); (se suele usar para camara en primera persona movil)
     //por ejemplo: posCamara + glm::vec3(1,0,0) para mirar a X+
     View = glm::lookAt(OBS, VRP, up);
  }
  else{ //tercera persona angles Euler
     View = glm::translate(View, glm::vec3(0,0,-2*radiEsc)); //2*radiEsc = d
     //View = glm::rotate(View, -angleZ, glm::vec3(0,0,1)); //angleZ = psi = p chunga  (no se suele usar, modifica el up)
     View = glm::rotate(View, angleX, glm::vec3(1,0,0)); //angleX = theta = circulo con barra
     View = glm::rotate(View, -angleY, glm::vec3(0,1,0)); //angleY = tridente
     View = glm::translate(View, -centreEsc); //== -vrp
  }
  glUniformMatrix4fv (viewLoc, 1, GL_FALSE, &View[0][0]);
  
  //calcul focus
  glm::vec3 foc;
  if(RespectoSCA) foc = glm::vec3(View*glm::vec4(posFocus,1.0));
  else if(RespectoModeloTG) foc = glm::vec3(View*TG*glm::vec4(posFocus,1.0));
  else if(RespectoSCO) foc = glm::vec3(glm::vec4(posFocus,1.0));
  glUniform3fv (posFocusLoc, 1, &foc[0]);
}

void MyGLWidget::keyPressEvent(QKeyEvent* event)
{
  makeCurrent();
  switch (event->key()){
  case Qt::Key_S: {
    if(legoPos.z < 6)
    legoPos += glm::vec3(0.f,0.f,0.2f);
    break;
  }
  case Qt::Key_W: {
    if(legoPos.z > 1)
    legoPos -= glm::vec3(0.f,0.f,0.2f);
    break;
  }
  default: event->ignore(); break;
  }
  update();
}

void MyGLWidget::mousePressEvent (QMouseEvent *e)
{
  xClick = e->x();
  yClick = e->y();
  if (e->button() & Qt::LeftButton &&
      ! (e->modifiers() & (Qt::ShiftModifier|Qt::AltModifier|Qt::ControlModifier)))
  {
    DoingInteractive = ROTATE;
  }
  if (e->button() & Qt::RightButton &&
      ! (e->modifiers() & (Qt::ShiftModifier|Qt::AltModifier|Qt::ControlModifier)))
  {
    ZOOM_ACTIVE = 1;
  }
}

void MyGLWidget::mouseReleaseEvent( QMouseEvent *)
{
  DoingInteractive = NONE;
  ZOOM_ACTIVE = 0;
}

void MyGLWidget::mouseMoveEvent(QMouseEvent *e)
{
  makeCurrent();
  if (DoingInteractive == ROTATE)
  {
    angleY += (e->x() - xClick) * M_PI / 180.0; //horizontal
    angleX += (yClick - e->y()) * M_PI / 180.0; //vertical
    viewTransform ();
  }
  if (ZOOM_ACTIVE == 1)
  {
    if(angle < float(M_PI/2)-0.1 and (e->y() - yClick) > 0){
        angle -= (yClick - e->y()) * M_PI / (180.0*5);
    
        //Per enviar zoom a slider del 0 al 100
        int z = ((angle-0.1)*100)/(float(M_PI)/2 - 0.2);
        emit(enviaZoom(z));
    }
    if(angle > 0.1 and (e->y() - yClick) < 0){
        angle -= (yClick - e->y()) * M_PI / (180.0*5);
	
        //Per enviar zoom a slider del 0 al 100
        int z = ((zoom-0.1)*100)/(float(M_PI)/2 - 0.2);
        emit(enviaZoom(z));
    }
    viewTransform ();
    projectTransform();
  }
  xClick = e->x();
  yClick = e->y();
  update ();
}

void MyGLWidget::calculaCapsaModel (Model &m, float &escala, glm::vec3 &centreBase)
{
  // Càlcul capsa contenidora i valors transformacions inicials
  float minx, miny, minz, maxx, maxy, maxz;
  minx = maxx = m.vertices()[0];
  miny = maxy = m.vertices()[1];
  minz = maxz = m.vertices()[2];
  for (unsigned int i = 3; i < m.vertices().size(); i+=3)
  {
    if (m.vertices()[i+0] < minx)
      minx = m.vertices()[i+0];
    if (m.vertices()[i+0] > maxx)
      maxx = m.vertices()[i+0];
    if (m.vertices()[i+1] < miny)
      miny = m.vertices()[i+1];
    if (m.vertices()[i+1] > maxy)
      maxy = m.vertices()[i+1];
    if (m.vertices()[i+2] < minz)
      minz = m.vertices()[i+2];
    if (m.vertices()[i+2] > maxz)
      maxz = m.vertices()[i+2];
  }
  escala = 1.0/(maxy-miny);
  centreBase[0] = (minx+maxx)/2.0;
  centreBase[1] = miny;
  centreBase[2] = (minz+maxz)/2.0;
}
